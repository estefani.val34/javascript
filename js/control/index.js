$(document).ready(function(){
    $("#div1").click(function(event){
        //alert("hola");
        //$(this).html("<p>Hola</p>");
        $(this).append("<p>añadido</p>");
    });

    $("#myList").click(function(event){
        $("#myList li:first").clone().appendTo("#myList");
        $("#myList li:last").clone().appendTo("#myList");
        $("#div1").hide(2000, function(){
            $("#div1").show();
        });
    });

    $("#div1 a").click(function(event){
        //stop its default behaviour
        event.preventDefault();
        $(this).attr("href", "http://www.google.com");
    });
    
    $("#div1 a").click(function(event){
        $("#div1").remove();
    });

    let sum = 0;
    let arr = [1,2,3,4,5];
    $.each(arr, function(index, value){
        sum += value;
    });
    console.log(sum);

    $("#myList li").each(function(){
        let li = $(this).text();
        console.log(li);
    });

    $("#div2").on("mouseenter", function(){
        console.log("in");
        $(this).css("background-color", "yellow");
        $("<div></div>").appendTo("body");
    });

    $("#div2").on("mouseout", function(){
        console.log("out");
        $(this).css("background-color", "brown");
    });

});